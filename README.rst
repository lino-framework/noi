========================
The ``lino_noi`` package
========================


- End-user documentation: https://using.lino-framework.org/apps/noi
- Developer documentation: https://dev.lino-framework.org/apps/noi
- Demo site: https://noi1r.lino-framework.org
- Source code: https://gitlab.com/lino-framework/noi
- Changelog: https://dev.lino-framework.org/changes
- Author: https://www.synodalsoft.net

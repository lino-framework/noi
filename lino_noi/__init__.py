# -*- coding: UTF-8 -*-
# Copyright 2014-2022 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This is the main module of Lino Noi.

.. autosummary::
   :toctree:

   lib


"""


__version__ = "25.2.2"

# srcref_url = 'https://gitlab.com/lino-framework/noi/blob/master/%s'
# doc_trees = ['docs']

# intersphinx_urls = dict(docs="https://noi.lino-framework.org")

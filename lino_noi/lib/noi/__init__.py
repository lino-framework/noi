# -*- coding: UTF-8 -*-
# Copyright 2014-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
The main plugin for Lino Noi.

.. autosummary::
   :toctree:

    fixtures.linotickets
    migrate

"""

from lino.api.ad import Plugin


# class Plugin(Plugin):
